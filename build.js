var {list} = require('./urls.js');
var app = document.querySelector('#app');
var {check} = require('./server.js');


window.onload = function(){
    
    for(i=0;i<list.length;i++){
        var container = document.createElement('div');
        container.className = "entry";
        container.innerHTML = `<p> <a href="${list[i]}">${list[i]} </a></p> <div class="loading"></div>`
        container.setAttribute("test-url", list[i]);
        app.appendChild(container);

        container.addEventListener("onclick", check(container));



    }
}
