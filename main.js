const {app, BrowserWindow} = require('electron');
var fs = require('fs'); // Load the File System to execute our common tasks (CRUD)
var path = require('path')

  
  function createWindow () {
    // Create the browser window.
    win = new BrowserWindow(
      {
        width: 800, 
        height: 600,
        icon: path.join(__dirname, 'xlc_icon.png')
      })

    // and load the index.html of the app.
    win.loadFile('index.html');

  }
  
  app.on('ready', createWindow)