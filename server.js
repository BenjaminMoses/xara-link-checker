var xhr = require("xhr");

function check(el){
    var URL = el.getAttribute('test-url');

    var xhr = require("xhr")

    xhr({
        method: "GET",
        url: URL
    }, function (err, resp, body) {
        if(resp.statusCode == 200){
            el.classList.add("pass");
        }else if(resp.statusCode == 404){
            el.classList.add("fail");
        }
    })
   return true;
}

module.exports.check = check;